/* eslint-disable */
// @ts-nocheck
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

interface BusData {
  angle: number;
  bus_depot_id: number;
  bus_depot_name: string;
  bus_id: number;
  bus_plate_number: string;
  create_date: string;
  distance: number;
  latitude: number;
  longitude: number;
  locality_code: string;
  record_id: number;
  route_direction_code: string;
  route_id: number;
  route_name: string;
  route_number: string;
  speed: number;
  terminal_date: string;
  terminal_model: string;
  terminal_serial_number: string;
  trip_id: number;
  trip_number: number;
}

export const busesApi = createApi({
  reducerPath: "busesApi",
  baseQuery: fetchBaseQuery({ baseUrl: "http://13.92.115.208:3033/" }),
  endpoints: (builder) => ({
    getBuses: builder.query<BusData[], string>({
      query: () => `getAll`,
      providesTags: (result) =>
        result
          ? [
              ...result.map(({ bus_id }) => ({ type: "Buses", bus_id })),
              { type: "Buses", id: "LIST" },
            ]
          : [{ type: "Buses", id: "LIST" }],
    }),
    updateBuses: builder.mutation({
      query: (body) => ({
        url: "updateBus",
        method: "POST",
        body,
      }),
      invalidatesTags: [{ type: "Buses", id: "LIST" }],
    }),
  }),
});

export const { useGetBusesQuery, useUpdateBusesMutation } = busesApi;
