/* eslint-disable */
// @ts-nocheck
import React, { useEffect } from "react";
import { YMaps, Map, Placemark } from "@pbe/react-yandex-maps";
import { useGetBusesQuery, useUpdateBusesMutation } from "./services/buses";

interface Bus {
  latitude: number;
  longitude: number;
}

function App() {
  const { data, isLoading } = useGetBusesQuery();
  const [updateBuses] = useUpdateBusesMutation();

  const center: [number, number] = [51.2088128, 51.3769565];

  useEffect(() => {
    const handleUpdateBuses = async (data: Bus[]) => {
      try {
        await updateBuses(data).unwrap();
      } catch (error) {
        console.error("Error updating buses:", error);
      }
    };

    const intervalId = setInterval(() => {
      if (!isLoading && data) {
        handleUpdateBuses(data);
      }
    }, 3000);

    return () => clearInterval(intervalId); // Cleanup interval on component unmount
  }, [data, isLoading, updateBuses]);

  if (isLoading) {
    return (
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          height: "100vh",
        }}
      >
        <h1>Loading...</h1>
      </div>
    );
  }

  return (
    <main>
      <YMaps>
        <Map
          width={"100%"}
          height={"100vh"}
          defaultState={{ center, zoom: 14 }}
        >
          {data &&
            data.map((bus, index) => (
              <Placemark
                key={index}
                geometry={[bus.latitude, bus.longitude]}
                properties={{
                  hintContent: "",
                  balloonContentHeader: "",
                  balloonContentBody: "",
                  balloonOpen: true,
                }}
              />
            ))}
        </Map>
      </YMaps>
    </main>
  );
}

export default App;
/* eslint-disable padded-blocks */