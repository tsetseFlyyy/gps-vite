import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { busesApi } from "../services/buses";

export const store = configureStore({
  reducer: {
    [busesApi.reducerPath]: busesApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(busesApi.middleware),
});

setupListeners(store.dispatch);
